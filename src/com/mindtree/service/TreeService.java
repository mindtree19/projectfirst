package com.mindtree.service;

import java.util.Scanner;

import com.mindtree.entity.Tree;

public class TreeService {
	public static Scanner sc = new Scanner(System.in);
	static Tree[] array;

	public static void AddTree(int n) {
		array = new Tree[n];
		for (int i = 0; i < n; i++) {
			System.out.println("Enter a tree id" + (i + 1));
			int id = sc.nextInt();
			sc.nextLine();
			System.out.println("Enter a tree name" + (i + 1));
			String name = sc.nextLine();
			System.out.println("Enter a tree age" + (i + 1));
			int age = sc.nextInt();
			System.out.println("Enter a tree hieght" + (i + 1));
			int height = sc.nextInt();
			array[i] = new Tree(id, name, age, height);

		}
		// TODO Auto-generated method stub

	}

	public static void displayTree() {
		// TODO Auto-generated method stub
		for (int i = 0; i < array.length; i++) {
			System.out.println(
					array[i].getId() + " " + array[i].getName() + " " + array[i].getHeight() + " " + array[i].getAge());
		}

	}

	public static void sortTreeByName() {
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = 0; j < array.length - i - 1; j++) {
				if (array[j].getName().compareTo(array[j + 1].getName()) > 0) {
					Tree temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}

	}

	public static void diplayPositionTree(String name) {
		// TODO Auto-generated method stub
		int lower = 0;
		int upper = array.length - 1;
		int middle = (lower + upper) / 2;
		if (array[middle].getName().equalsIgnoreCase(name)) {
			System.out.println("Search name is found the position is" + " " + middle);
		} else if (array[middle].getName().compareToIgnoreCase(name) > 0) {
			for (int i = 0; i < middle; i++) {
				if (array[i].getName().equalsIgnoreCase(name)) {
					System.out.println("Search name is found the position is" + " " + i);
					break;
				}
			}
		} else {
			for (int i = middle + 1; i < array.length; i++) {
				if (array[i].getName().equalsIgnoreCase(name)) {
					System.out.println("Search name is found the position is" + " " + i);
					break;
				}
			}
		}
	}

	public static void updateTreeAgeHeight(int ids, int heights, int ages) {
		// TODO Auto-generated method stub
		Tree[] temp = sortByIds(array);
		int lower = 0;
		int upper = temp.length - 1;
		int middle = (lower + upper) / 2;
		if (temp[middle].getId() == ids) {
			temp[middle].setHeight(heights);
			temp[middle].setAge(ages);

		} else if (temp[middle].getId() > ids) {
			for (int i = 0; i < middle; i++) {
				if (temp[i].getId() == ids) {
					temp[middle].setHeight(heights);
					temp[middle].setAge(ages);
					break;
				}
			}
		} else {
			for (int i = middle + 1; i < temp.length; i++) {
				if (temp[i].getId() == ids) {
					temp[middle].setHeight(heights);
					temp[middle].setAge(ages);
					break;
				}
			}
		}

	}

	private static Tree[] sortByIds(Tree[] array2) {
		// TODO Auto-generated method stub
		for (int i = 0; i < array2.length - 1; i++) {
			for (int j = 0; j < array2.length - i - 1; j++) {
				if (array2[j].getId() > array2[j + 1].getId()) {
					Tree temp = array2[j];
					array2[j] = array2[j + 1];
					array2[j + 1] = temp;
				}
			}
		}
		return array2;
	}

}
