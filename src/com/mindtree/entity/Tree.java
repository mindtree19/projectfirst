package com.mindtree.entity;

public class Tree {
	private int id;
	private String name;
	private int age;
	private int height;
	public Tree() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Tree(int id, String name, int age, int height) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.height = height;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	

}
